package com.example.exchange_rate;

import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;  
import android.view.View;  
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
public class ExchangeRate extends Fragment {  
	
	
	//Оголошення
	TextView buyUSD;
    TextView saleUSD;    
    TextView buyEUR;
    TextView saleEUR;    
    TextView buyRUR;
    TextView saleRUR;
    TextView current_time;
    ProgressDialog pd;
    
    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mSqLiteDatabase;
	private Handler h;  

    @Override
    public void onPause() {
        super.onPause();
    }

     @Override  
     public View onCreateView(LayoutInflater inflater, ViewGroup container,  
               Bundle savedInstanceState) {  
          // TODO Auto-generated method stub  
    	 
    	 
    	
    	 
    	 View view = inflater.inflate(R.layout.exchange_rate, container, false);
    	 
    	 buyUSD = (TextView) view.findViewById(R.id.textView3);

         saleUSD = (TextView) view.findViewById(R.id.textView4);
         
         buyEUR = (TextView) view.findViewById(R.id.textView5);
         saleEUR = (TextView) view.findViewById(R.id.textView6);
         
         buyRUR = (TextView) view.findViewById(R.id.textView7);
         saleRUR = (TextView) view.findViewById(R.id.textView8);
         
         
         current_time = (TextView) view.findViewById(R.id.textView9);
         
         mDatabaseHelper = new DatabaseHelper(getActivity(), "mydatabase.db", null, 1);
         mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();  

     	JSONObject json = null;
         String str = "";
         HttpResponse response;
         HttpClient myClient = new DefaultHttpClient();
         HttpPost myConnection = new HttpPost("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5");
          
         try {
             response = myClient.execute(myConnection);
             str = EntityUtils.toString(response.getEntity(), "UTF-8");
             String currentDateTimeString = (String) android.text.format.DateFormat.format("yy-MM-dd kk:mm:ss", new Date());
             String currentDateTimeStringBD = (String) android.text.format.DateFormat.format("yy-MM-dd kk:mm:ss", new Date());
             current_time.setText(currentDateTimeString);  
             
             try{
                 JSONArray jArray = new JSONArray(str);
                 json = jArray.getJSONObject(0);
                  
                 buyRUR.setText(json.getString("buy"));
                 saleRUR.setText(json.getString("sale"));
                 
                 json = jArray.getJSONObject(1);
                 buyEUR.setText(json.getString("buy"));
                 saleEUR.setText(json.getString("sale"));
                 
                 json = jArray.getJSONObject(2);
                 buyUSD.setText(json.getString("buy"));
                 saleUSD.setText(json.getString("sale"));
                 
                 ContentValues newValues = new ContentValues();
                 newValues.put(DatabaseHelper.CCY_COLUMN, "USD");
                 newValues.put(DatabaseHelper.BUY_COLUMN, buyUSD.getText().toString());
                 newValues.put(DatabaseHelper.SALE_COLUMN, saleUSD.getText().toString());
                 newValues.put(DatabaseHelper.CUR_DATE, currentDateTimeStringBD);
                 mSqLiteDatabase.insert("kyrs", null, newValues);
                 newValues.clear();
                 
                 newValues.put(DatabaseHelper.CCY_COLUMN, "RUR");
                 newValues.put(DatabaseHelper.BUY_COLUMN, buyRUR.getText().toString());
                 newValues.put(DatabaseHelper.SALE_COLUMN, saleRUR.getText().toString());
                 newValues.put(DatabaseHelper.CUR_DATE, currentDateTimeStringBD);
                 mSqLiteDatabase.insert("kyrs", null, newValues);
                 newValues.clear();
                 
                 newValues.put(DatabaseHelper.CCY_COLUMN, "EUR");
                 newValues.put(DatabaseHelper.BUY_COLUMN, buyEUR.getText().toString());
                 newValues.put(DatabaseHelper.SALE_COLUMN, saleEUR.getText().toString());
                 newValues.put(DatabaseHelper.CUR_DATE, currentDateTimeStringBD);
                 mSqLiteDatabase.insert("kyrs", null, newValues);
                 newValues.clear();
                  
             } catch ( JSONException e) {
            	 Context con = getActivity();
            	 AlertDialog.Builder builder = new AlertDialog.Builder(con);
            		builder.setTitle("Увага!")
            				.setMessage(e.toString())
            				.setIcon(R.drawable.ic_launcher)
            				.setCancelable(false)
            				.setNegativeButton("ОК",
            						new DialogInterface.OnClickListener() {
            							public void onClick(DialogInterface dialog, int id) {
            								dialog.cancel();
            							}
            						});
            		AlertDialog alert = builder.create();
            		alert.show();              
             }
         } catch (ClientProtocolException e) {
        	 Context con = getActivity();
        	 AlertDialog.Builder builder = new AlertDialog.Builder(con);
        		builder.setTitle("Увага!")
        				.setMessage("Немає підключення до інтернету!")
        				.setIcon(R.drawable.ic_launcher)
        				.setCancelable(false)
        				.setNegativeButton("ОК",
        						new DialogInterface.OnClickListener() {
        							public void onClick(DialogInterface dialog, int id) {
        								dialog.cancel();
        							}
        						});
        		AlertDialog alert = builder.create();
        		alert.show();
         } catch (IOException e) {
             e.printStackTrace();
         }
          
          
         
         mSqLiteDatabase.close();
         Button button1 = (Button) view.findViewById(R.id.button1);
         button1.setOnClickListener(new OnClickListener(){
        	 @Override
        		public void onClick(View v) {
        			// TODO Auto-generated method stub
        		 mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();  
        		 JSONObject json = null;
                 String str = "";
                 HttpResponse response;
                 HttpClient myClient = new DefaultHttpClient();
                 HttpPost myConnection = new HttpPost("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5");
                  
                 try {
                     response = myClient.execute(myConnection);
                     str = EntityUtils.toString(response.getEntity(), "UTF-8");
                     String currentDateTimeString = (String) android.text.format.DateFormat.format("yy-MM-dd kk:mm:ss", new Date());
                     String currentDateTimeStringBD = (String) android.text.format.DateFormat.format("yy-MM-dd kk:mm:ss", new Date());
                     current_time.setText(currentDateTimeString);  
                     try{
                         JSONArray jArray = new JSONArray(str);
                         json = jArray.getJSONObject(0);
                          
                         buyRUR.setText(json.getString("buy"));
                         saleRUR.setText(json.getString("sale"));
                         
                         json = jArray.getJSONObject(1);
                         buyEUR.setText(json.getString("buy"));
                         saleEUR.setText(json.getString("sale"));
                         
                         json = jArray.getJSONObject(2);
                         buyUSD.setText(json.getString("buy"));
                         saleUSD.setText(json.getString("sale"));
                         
                         ContentValues newValues = new ContentValues();

                         newValues.put(DatabaseHelper.CCY_COLUMN, "USD");
                         newValues.put(DatabaseHelper.BUY_COLUMN, buyUSD.getText().toString());
                         newValues.put(DatabaseHelper.SALE_COLUMN, saleUSD.getText().toString());
                         newValues.put(DatabaseHelper.CUR_DATE, currentDateTimeStringBD);
                         mSqLiteDatabase.insert("kyrs", null, newValues);
                         newValues.clear();
                         
                         newValues.put(DatabaseHelper.CCY_COLUMN, "RUR");
                         newValues.put(DatabaseHelper.BUY_COLUMN, buyRUR.getText().toString());
                         newValues.put(DatabaseHelper.SALE_COLUMN, saleRUR.getText().toString());
                         newValues.put(DatabaseHelper.CUR_DATE, currentDateTimeStringBD);
                         mSqLiteDatabase.insert("kyrs", null, newValues);
                         newValues.clear();
                         
                         newValues.put(DatabaseHelper.CCY_COLUMN, "EUR");
                         newValues.put(DatabaseHelper.BUY_COLUMN, buyEUR.getText().toString());
                         newValues.put(DatabaseHelper.SALE_COLUMN, saleEUR.getText().toString());
                         newValues.put(DatabaseHelper.CUR_DATE, currentDateTimeStringBD);
                         mSqLiteDatabase.insert("kyrs", null, newValues);
                         newValues.clear();
                          
                     } catch ( JSONException e) {
                    	 Context con = getActivity();
                    	 AlertDialog.Builder builder = new AlertDialog.Builder(con);
                    		builder.setTitle("Увага!")
                    				.setMessage(e.toString())
                    				.setIcon(R.drawable.ic_launcher)
                    				.setCancelable(false)
                    				.setNegativeButton("ОК",
                    						new DialogInterface.OnClickListener() {
                    							public void onClick(DialogInterface dialog, int id) {
                    								dialog.cancel();
                    							}
                    						});
                    		AlertDialog alert = builder.create();
                    		alert.show();                 
                     }
                 } catch (ClientProtocolException e) {
                	 Context con = getActivity();
                	 AlertDialog.Builder builder = new AlertDialog.Builder(con);
                		builder.setTitle("Увага!")
                				.setMessage("Немає підключення до інтернету!")
                				.setIcon(R.drawable.ic_launcher)
                				.setCancelable(false)
                				.setNegativeButton("ОК",
                						new DialogInterface.OnClickListener() {
                							public void onClick(DialogInterface dialog, int id) {
                								dialog.cancel();
                							}
                						});
                		AlertDialog alert = builder.create();
                		alert.show();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
                  
                  
                 
                 mSqLiteDatabase.close();
        		}
         });
         
          return view;
     }  
   
}
  