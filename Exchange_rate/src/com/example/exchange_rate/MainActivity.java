package com.example.exchange_rate;

import android.app.ActionBar;  
import android.app.ProgressDialog;
import android.app.ActionBar.Tab;  
import android.app.FragmentTransaction;  
import android.content.DialogInterface;
import android.os.Bundle;  
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;  
import android.support.v4.view.ViewPager;  
import android.view.Menu;
import android.view.MenuItem;
//import android.view.View;

 public class MainActivity extends FragmentActivity implements ActionBar.TabListener{  
      ActionBar actionbar;  
      ViewPager viewpager;  
      FragmentPageAdapter ft;  
     // private View rootView;
      
      @Override  
      protected void onCreate(Bundle savedInstanceState) {  
           super.onCreate(savedInstanceState);  
           setContentView(R.layout.activity_main);  
           
           StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
           .detectAll()
           .penaltyLog()
           //.penaltyDialog()
           .build());
        
           StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll()
           .penaltyLog()
           .build());
           
           
           viewpager = (ViewPager) findViewById(R.id.pager);  
           ft = new FragmentPageAdapter(getSupportFragmentManager());  
           actionbar = getActionBar();  
           viewpager.setAdapter(ft);  
           actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);  
           actionbar.addTab(actionbar.newTab().setText("Курс валют").setTabListener(this));  
           actionbar.addTab(actionbar.newTab().setText("Конвертер").setTabListener(this));  
           actionbar.addTab(actionbar.newTab().setText("Графік").setTabListener(this));  
           viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {  
                @Override  
                public void onPageSelected(int arg0) {  
                actionbar.setSelectedNavigationItem(arg0);  
                }  
                @Override  
                public void onPageScrolled(int arg0, float arg1, int arg2) {  
                     // TODO Auto-generated method stub  
                }  
                @Override  
                public void onPageScrollStateChanged(int arg0) {  
                     // TODO Auto-generated method stub  
                }  
           });  
           final ProgressDialog progress = new ProgressDialog(this);
   	    progress.setTitle("Connecting");
   	    progress.setMessage("Please wait while we connect to server...");
   	    progress.show();

   	    Runnable progressRunnable = new Runnable() {

   	        @Override
   	        public void run() {
   	            progress.cancel();
   	        }
   	    };

   	    Handler pdCanceller = new Handler();
   	    pdCanceller.postDelayed(progressRunnable, 5000);
   	    
   	    progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
   	        @Override
   	        public void onCancel(DialogInterface dialog) {
   	            //theLayout.setVisibility(View.GONE);
   	        }
   	    });
           
           
      }  
      
      @Override  
      public void onTabReselected(Tab tab, FragmentTransaction ft) {  
           // TODO Auto-generated method stub  
      }  
      @Override  
      public void onTabSelected(Tab tab, FragmentTransaction ft) {  
           viewpager.setCurrentItem(tab.getPosition());  
      }  
      @Override  
      public void onTabUnselected(Tab tab, FragmentTransaction ft) {  
           // TODO Auto-generated method stub  
      }  

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
