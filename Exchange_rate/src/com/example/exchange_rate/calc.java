package com.example.exchange_rate;

import java.math.BigDecimal;

import android.os.Bundle;  
import android.support.v4.app.Fragment;  
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;  
import android.view.View;  
import android.view.View.OnClickListener;
import android.view.ViewGroup;  
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
public class Calc extends Fragment {  
	
	TextView exchanche_value;
	Spinner spinner1,spinner2;
	EditText editText1;
	TextView result;
	
	/*InputFilter customFilter = new InputFilter() {   
		@Override
		public CharSequence filter(CharSequence arg0, int arg1, int arg2, Spanned arg3, int arg4, int arg5){  
			for (int i = arg1; i < arg2; i++) {   
				if( arg0.charAt(i) != '0' || arg0.charAt(i) != '1' || arg0.charAt(i) != '2' 
						|| arg0.charAt(i) != '3' || arg0.charAt(i) != '4' || arg0.charAt(i) != '5'
						|| arg0.charAt(i) != '6' || arg0.charAt(i) != '7' || arg0.charAt(i) != '8'
						|| arg0.charAt(i) != '8' || arg0.charAt(i) != '9')  {   
					return "";   
				}
			}
			return null;   
		}
	};*/
     @Override  
     public View onCreateView(LayoutInflater inflater, ViewGroup container,  
               Bundle savedInstanceState) {  
          // TODO Auto-generated method stub  
    	 
    	 
    	 
    	 View view = inflater.inflate(R.layout.calc, container,false);  
          
          spinner1 = (Spinner) view.findViewById(R.id.spinner1);
          spinner2 = (Spinner) view.findViewById(R.id.spinner2);
          editText1 = (EditText) view.findViewById(R.id.editText1);
          //editText1.setFilters(new InputFilter[]{ customFilter});
          result = (TextView) view.findViewById(R.id.textView1);
          
          
          Button button = (Button) view.findViewById(R.id.button1);
          button.setOnClickListener(new OnClickListener() {
        	 
        	  

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	            	double Exchanche_value;
	            	String ccy = spinner1.getSelectedItem().toString();  
	            	String type_chois = spinner2.getSelectedItem().toString();  
	            	int ccy_chois = 0;
	            	if ((ccy).compareToIgnoreCase("USD") == 0)   ccy_chois = 1;
	            	if ((ccy).compareToIgnoreCase("RUR") == 0)   ccy_chois = 2;
	            	if ((ccy).compareToIgnoreCase("EUR") == 0)   ccy_chois = 3;
	            	int type;
	            	//(string1).compareToIgnoreCase(getString(string2)) == 0
	            	//if ((type_chois).compareToIgnoreCase("Купівля") == 0) type = 1; else type = 2;
	            	double count = Double.parseDouble(editText1.getText().toString());
	            	
	            	if ((type_chois).compareToIgnoreCase("Купівля") == 0){//������, returns UAH
	         			switch (ccy_chois){
	         			case 0:{
	         				break;
	         			}
	         			case 1:{
	         				exchanche_value = (TextView) getActivity().findViewById(R.id.textView3);
	         				Exchanche_value = Double.parseDouble((String) exchanche_value.getText());        				
	         				result.setText(String.valueOf(roundUp(count*Exchanche_value,2))+" UAH");
	         				break;
	         			}
	         			case 2:{
	         				exchanche_value = (TextView) getActivity().findViewById(R.id.textView7);
	         				Exchanche_value = Double.parseDouble((String) exchanche_value.getText());
	         				result.setText(String.valueOf(roundUp(count*Exchanche_value,2))+" UAH");
	         				break;
	         			}
	         			case 3:{
	         				exchanche_value = (TextView) getActivity().findViewById(R.id.textView5);
	         				Exchanche_value = Double.parseDouble((String) exchanche_value.getText());
	         				result.setText(String.valueOf(roundUp(count*Exchanche_value,2))+" UAH");
	         				break;
	         			}
	         			}
	         		}else{//������
	         			switch (ccy_chois){
	         			case 0:{
	         				break;
	         			}
	         			case 1:{
	         				exchanche_value = (TextView) getActivity().findViewById(R.id.textView4);
	         				Exchanche_value = Double.parseDouble((String) exchanche_value.getText());
	         				result.setText(String.valueOf(roundUp(count/Exchanche_value,2))+" USD");
	         				break;
	         			}
	         			case 2:{
	         				exchanche_value = (TextView) getActivity().findViewById(R.id.textView8);
	         				Exchanche_value = Double.parseDouble((String) exchanche_value.getText());
	         				result.setText(String.valueOf(roundUp(count/Exchanche_value,2))+" RUR");
	         				break;
	         			}
	         			case 3:{
	         				exchanche_value = (TextView) getActivity().findViewById(R.id.textView6);
	         				Exchanche_value = Double.parseDouble((String) exchanche_value.getText());
	         				result.setText(String.valueOf(roundUp(count/Exchanche_value,2))+" EUR");
	         				break;
	         			}
	         			}
	         		}
	            }	
          });
          
          return view;
     }
     public BigDecimal roundUp(double value, int digits){
    	    return new BigDecimal(""+value).setScale(digits, BigDecimal.ROUND_HALF_UP);
    } 
     
}  
